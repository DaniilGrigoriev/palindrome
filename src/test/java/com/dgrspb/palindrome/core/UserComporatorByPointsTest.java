package com.dgrspb.palindrome.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by daniil.grigoriev on 22.04.2016.
 */
public class UserComporatorByPointsTest {
    @Test
    public void TestCompare() throws Exception {
        UserStats user0 = new UserStats(new User("user0"), 50);
        UserStats user1 = new UserStats(new User("user1"), 100);
        UserStats user2 = new UserStats(new User("user2"), 200);
        UserStats user3 = new UserStats(new User("user3"), 200);
        UserStats user4 = new UserStats(new User("user3"), 200);

        UserStatsComporatorByPoints comporator = new UserStatsComporatorByPoints();

        assertEquals(1, comporator.compare(user0, user1));
        assertEquals(-1, comporator.compare(user1, user0));

        assertEquals(1, comporator.compare(user0, user2));
        assertEquals(-1, comporator.compare(user2, user0));

        assertEquals(-1, comporator.compare(user2, user3));
        assertEquals(1, comporator.compare(user3, user2));

        assertEquals(0, comporator.compare(user3, user4));
        assertEquals(0, comporator.compare(user4, user3));
    }

    @Test(expected = NullPointerException.class)
    public void TestCompareNullPointerException1() throws Exception {
        UserStats user = new UserStats(new User("user"), 100);
        UserStatsComporatorByPoints comporator = new UserStatsComporatorByPoints();
        comporator.compare(user, null);
    }

    @Test(expected = NullPointerException.class)
    public void TestCompareNullPointerException2() throws Exception {
        UserStats user = new UserStats(new User("user"), 100);
        UserStatsComporatorByPoints comporator = new UserStatsComporatorByPoints();
        comporator.compare(null, user);
    }
}