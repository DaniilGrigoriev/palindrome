package com.dgrspb.palindrome.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by user on 24.04.2016.
 */
@RunWith(Parameterized.class)
public class PointCalculatorBaseTest {

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {true, "топот"},
                {false, "топот2"},
                {true, "а роза упала на лапу АЗОРА"},
                {true, "а ро,за упа,ла на л,апу АЗОРА"},
                {true, "На море ищущие роман"},
                {false, "123"}
        });

    }

    private boolean expect;
    private String text;

    public PointCalculatorBaseTest(boolean expect, String text) {
        this.expect = expect;
        this.text = text;
    }

    @Test
    public void test() {
        PointCalculatorBase calc = new PointCalculatorBase();
        String polindromsWithoutDelim = calc.getStringWithoutDelim(new Phrase(text));
        assertEquals(expect, calc.check(polindromsWithoutDelim));
    }

    @Test
    public void calculatePoint() throws Exception {
        Integer expectPoint = 0;
        Phrase phrase;
        List<Phrase> list = new LinkedList<>();
        PointCalculatorBase calc = new PointCalculatorBase();

        phrase = new Phrase("топот");
        expectPoint += phrase.getLength();
        list.add(phrase);

        phrase = new Phrase("топот");
        expectPoint += 0;
        list.add(phrase);

        phrase = new Phrase("топот222");
        expectPoint += 0;
        list.add(phrase);

        phrase = new Phrase("а роза упала на лапу АЗОРА");
        expectPoint += phrase.getLength();
        list.add(phrase);

        phrase = new Phrase("а роза упала на лапу, АЗОРА");
        expectPoint += 0;
        list.add(phrase);

        assertEquals(expectPoint, calc.calculatePoint(list));
    }

}