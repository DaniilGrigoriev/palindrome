package com.dgrspb.palindrome.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by daniil.grigoriev on 21.04.2016.
 */
public class PhraseTest {
    @Test
    public void equalsTest() throws Exception {
        Phrase phrase1 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls");
        Phrase phrase2 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls");
        Phrase phrase3 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls");
        Phrase phrase4 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls!!!!!!");

        assertEquals(phrase1.equals(phrase1), true);
        assertEquals(phrase1.equals(phrase2), true);
        assertEquals(phrase2.equals(phrase1), true);
        assertEquals(phrase1.equals(phrase3), true);
        assertEquals(phrase1.equals(phrase4), false);
        assertEquals(phrase1.equals(null), false);

    }

    @Test
    public void hashCodeTest() throws Exception {
        Phrase phrase1 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls");
        Phrase phrase2 = new Phrase("dslkfjsdlfjslkfjslkfjaslkfjsdlfhsdolfh3hkjkgjldkjfls");

        assertEquals(phrase1.hashCode(), phrase2.hashCode());
    }

}