package com.dgrspb.palindrome.core;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by user on 22.04.2016.
 */
public class GameTest {
    private static final String[] POLINDROMS = new String[]{
            "А щи - ПИЩА?",
            "?Яд ем как мед я!?",
            "Кит на море романтик",
            "АРГЕНТИНА манит негра",
            "Я иду с МЕЧЕМ, судия!!",
            "И любит Сева вестибюли!",
            "Удавы рвали лавры в АДУ!",
            "ЛЁША на полке клопа нашёл",
            "А роза упала на лапу АЗОРА",
            "Муза!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, Ранясь шилом опыта, ты помолишься на разум"
    };

    private PointCalculatorBase pointCalculator;
    private Game game;


    @Before
    public void setUp() throws Exception {

        pointCalculator = new PointCalculatorBase();
        game = new Game(pointCalculator);

    }

    @Before
    public void fillArrayPl() {

        PointCalculatorBase pointCalculatorLocal = new PointCalculatorBase();

        for (int i = 0; i < POLINDROMS.length; i++) {
            Phrase phrase = new Phrase(POLINDROMS[i]);
            String polindromsWithoutDelim = pointCalculatorLocal.getStringWithoutDelim(phrase);
            assertTrue(pointCalculatorLocal.check(polindromsWithoutDelim));
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetHistoryUser() throws Exception {

        game.registerUser("user1");
        game.action("user1", new Phrase(POLINDROMS[1]));
        game.action("user1", new Phrase("qaz123"));
        List<Phrase> history = game.getHistoryUser("user1");
        //Can`t edit
        history.add(new Phrase("11111"));
    }

    @Test
    public void testGetBoard() throws Exception {

        UserBoard board;

        for (int i = 0; i < 10; i++) {
            game.registerUser("user" + i);
            game.action("user" + i, new Phrase(POLINDROMS[i]));
            game.action("user" + i, new Phrase("qaz123"));
        }

        board = game.getUserBoard();
        assertEquals("user9", board.getUserStats().get(0).getLogin());
        assertEquals("user8", board.getUserStats().get(1).getLogin());
        assertEquals("user7", board.getUserStats().get(2).getLogin());
        assertEquals("user6", board.getUserStats().get(3).getLogin());
        assertEquals("user5", board.getUserStats().get(4).getLogin());
        assertEquals(5, board.getUserStats().size());

    }
}