package com.dgrspb.palindrome.core;

import com.dgrspb.palindrome.core.exception.UserNotRegister;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by user on 24.04.2016.
 */
public class UserStatsTest {
    public static UserStats userStats;
    public static Game game;
    public static Phrase phrase = new Phrase("а роза упала на лапу АЗОРА");

    @Before
    public void init() throws UserNotRegister {
        PointCalculator<Phrase> calculater = new PointCalculatorBase();
        game = new Game(calculater);
        game.registerUser("user1");
        game.action("user1", phrase);
        User user = game.getUser("user1");

        userStats = new UserStats(user, game);
        userStats.refresh();
    }

    @Test
    public void getName() throws Exception {
        assertEquals("user1", userStats.getLogin());
    }

    @Test
    public void getHistory() throws Exception {
        List<Phrase> listExpect = new LinkedList<>();
        listExpect.add(new Phrase("а роза упала на лапу АЗОРА"));

        assertEquals(listExpect, userStats.getHistory());
    }

    @Test
    public void getPoint() throws Exception {
        assertEquals(phrase.getLength(), userStats.getPoint());
    }

    @Test
    public void refresh() throws Exception {
        Integer pointExpect = userStats.getPoint();
        Phrase phrase = new Phrase("топот");
        pointExpect += phrase.getLength();
        game.action("user1", phrase);
        userStats.refresh();
        assertEquals(pointExpect, userStats.getPoint());
    }

}