package com.dgrspb.palindrome.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by user on 21.04.2016.
 */
public class UserTest {
    @Test
    public void equalsTest() throws Exception {
        User user1 = new User("user1");
        User user2 = new User("user1");
        User user3 = new User("user1");
        User user4 = new User("user2");

        assertEquals(user1.equals(user1), true);
        assertEquals(user1.equals(user2), true);
        assertEquals(user2.equals(user3), true);
        assertEquals(user1.equals(user3), true);
        assertEquals(user1.equals(user4), false);
        assertEquals(user1.equals(null), false);

    }

    @Test
    public void hashCodeTest() throws Exception {
        User user1 = new User("user1");
        User user2 = new User("user1");

        assertEquals(user1.hashCode(), user2.hashCode());
    }

}