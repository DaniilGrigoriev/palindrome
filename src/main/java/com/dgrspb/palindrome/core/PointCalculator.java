package com.dgrspb.palindrome.core;

import java.util.List;

/**
 * Created by user on 24.04.2016.
 */
public interface PointCalculator<T> {
    public Integer calculatePoint(List<T> history);
}
