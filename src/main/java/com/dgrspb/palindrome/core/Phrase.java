package com.dgrspb.palindrome.core;

/**
 * Created by user on 20.04.2016.
 */

/**
 * Immutable object
 */
public class Phrase {
    private String value;

    public String getValue() {
        return value;
    }

    public Phrase(String value) {
        this.value = value;
    }

    public Integer getLength() {
        return value.length();
    }

    public char[] getCharArray() {
        return value.toCharArray();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phrase phrase = (Phrase) o;
        return !(value != null ? !value.equals(phrase.value) : phrase.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Phrase{" +
                "value='" + value + '\'' +
                '}';
    }
}

