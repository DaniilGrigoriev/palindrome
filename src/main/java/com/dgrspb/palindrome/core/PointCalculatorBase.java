package com.dgrspb.palindrome.core;

import java.util.*;

/**
 * Created by user on 24.04.2016.
 */
public class PointCalculatorBase implements PointCalculator<Phrase> {
    private static Set<Character> charDelim = new HashSet(Arrays.asList(new Character[]{' ', ',', '.', ';', ':', '!', '?', '(', ')', '-'}));


    private static boolean checkByDelim(Character character) {
        return charDelim.contains(character);
    }

    String getStringWithoutDelim(Phrase phrase) {
        StringBuilder builder = new StringBuilder();
        for (Character character : phrase.getCharArray()) {
            if (checkByDelim(character)) {
                continue;
            }
            builder.append(Character.toUpperCase(character));
        }
        return builder.toString();
    }

    private String reverse(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = str.length() - 1; i >= 0 ; i--) {
            builder.append(str.charAt(i));
        }
        return builder.toString();
    }

    boolean check(String str) {
        return str.equals(reverse(str));
    }

    @Override
    public Integer calculatePoint(List<Phrase> history) {
        HashSet<String> uniqPalindroms = new HashSet<>();
        int point = 0;
        boolean isPalindrome;
        String stringWithoutDelim;
        for (Phrase phrase : history){
            stringWithoutDelim = getStringWithoutDelim(phrase);
            isPalindrome = check(stringWithoutDelim);
            if (isPalindrome) {
                if (uniqPalindroms.add(stringWithoutDelim)) {
                    point += phrase.getLength();
                }
            }
        }
        return point;
    }
}
