package com.dgrspb.palindrome.core;

import java.util.List;

/**
 * Created by user on 24.04.2016.
 */
public class UserBoard {
    private List<UserStats> userStats;

    public List<UserStats> getUserStats() {
        return userStats;
    }

    public void setUserStats(List<UserStats> userStats) {
        this.userStats = userStats;
    }
}
