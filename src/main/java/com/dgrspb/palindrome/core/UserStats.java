package com.dgrspb.palindrome.core;

import com.dgrspb.palindrome.core.exception.UserNotRegister;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by daniil.grigoriev on 22.04.2016.
 */
public final class UserStats {
    private User user;
    private GameSession game;
    private Integer point;
    private List<Phrase> hisotyUser;

    public UserStats(User user, GameSession game) {
        this.user = user;
        this.game = game;
        this.point = 0;
        List<Phrase> hisotyUser = new LinkedList<>();
    }

    public String getLogin() {
        return user.getLogin();
    }

    public List<Phrase> getHistory() {
        return hisotyUser;
    }

    public Integer getPoint() {
        return point;
    }

    public void refresh() throws UserNotRegister {
        point = game.getPointsUser(user.getLogin());
        hisotyUser = new LinkedList<>(game.getHistoryUser(user.getLogin()));
    }

    @Override
    public String toString() {
        return "UserStats{" +
                "login=" + getLogin() +
                ", actionCount=" + hisotyUser.size() +
                ", point=" + point +
                '}';
    }

    //For test
    UserStats(User user, Integer point) {
        this.user = user;
        this.point = point;
    }
}
