package com.dgrspb.palindrome.core;

import com.dgrspb.palindrome.core.exception.UserNotRegister;

import java.util.*;

/**
 * Created by user on 24.04.2016.
 */
public final class Game implements GameSession {
    public static final Integer DEFAULT_COUNT_USER_BOARD = 5;

    private Map<String, User> users;
    private Map<String, List<Phrase>> history;
    private PointCalculator<Phrase> objCalc;

    public Game(PointCalculator<Phrase> objCalc) {
        this.objCalc = objCalc;
        this.users = new HashMap<>();
        this.history = new HashMap<>();
    }

    @Override
    public boolean registerUser(String userName) {
        if (!users.containsKey(userName)) {
            users.put(userName, new User(userName));
            history.put(userName, new LinkedList<Phrase>());
            return true;
        } else {
            return false;
        }
    }

    public User getUser(String userName) {
        return users.get(userName);
    }

    @Override
    public void action(String userName, Phrase param) throws UserNotRegister {
        checkUserIsRegister(userName);
        history.get(userName).add(param);
    }

    @Override
    public List<Phrase> getHistoryUser(String userName) throws UserNotRegister {
        checkUserIsRegister(userName);
        List<Phrase> userHistory = history.get(userName);
        return Collections.unmodifiableList(userHistory);
    }

    private void checkUserIsRegister(String userName) throws UserNotRegister {
        if (!users.containsKey(userName)) {
            throw new UserNotRegister();
        }
    }

    @Override
    public Integer getPointsUser(String userName) throws UserNotRegister {
        checkUserIsRegister(userName);
        return objCalc.calculatePoint(Collections.unmodifiableList(history.get(userName)));
    }

    @Override
    public UserBoard getUserBoard() {
        Comparator<UserStats> comporator = new UserStatsComporatorByPoints();

        UserStats[] userStatsAll = new UserStats[users.size()];
        int i = 0;
        for (User user : users.values()) {
            userStatsAll[i] = new UserStats(user, this);
            try {
                userStatsAll[i].refresh();
            } catch (UserNotRegister e) {
                // It`s not possible
            }
            i++;
        }
        Arrays.sort(userStatsAll, comporator);
        List<UserStats> topUser = Arrays.asList(Arrays.copyOf(userStatsAll, DEFAULT_COUNT_USER_BOARD));
        UserBoard board = new UserBoard();
        board.setUserStats(topUser);
        return board;
    }
}
