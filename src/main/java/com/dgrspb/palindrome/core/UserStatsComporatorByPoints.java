package com.dgrspb.palindrome.core;

import java.util.Comparator;

/**
 * Created by daniil.grigoriev on 22.04.2016.
 */
class UserStatsComporatorByPoints implements Comparator<UserStats> {

    @Override
    public int compare(UserStats user1, UserStats user2) {
        //Reverse compare
        int result = Integer.compare(user2.getPoint(), user1.getPoint());
        if (result == 0) {
            result = user1.getLogin().compareTo(user2.getLogin());
        }
        return result;
    }
}
