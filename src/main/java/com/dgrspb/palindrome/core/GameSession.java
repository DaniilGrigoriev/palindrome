package com.dgrspb.palindrome.core;

import com.dgrspb.palindrome.core.exception.UserNotRegister;

import java.util.List;

/**
 * Created by user on 24.04.2016.
 */
public interface GameSession {
    public boolean registerUser(String userName);

    public User getUser(String userName);

    public void action(String userName, Phrase param) throws UserNotRegister;

    public List<Phrase> getHistoryUser(String userName) throws UserNotRegister;

    public Integer getPointsUser(String userName) throws UserNotRegister;

    public UserBoard getUserBoard();
}
